import { Component, OnInit } from '@angular/core';
import { UserlistComponent } from '../userlist/userlist.component';
import { FormGroup, FormBuilder ,Validators} from '@angular/forms';
import { UserserviceService } from '../service/userservice.service';
import { UserModel } from '../userlist/user.model';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  formvalue !: FormGroup;
  searchtext!:any;
  usermodel : UserModel = new UserModel();
  userdata !: any;
  showadd!: boolean;
  showupdate!:boolean;
  user!:any;
  userid!:any;
  selectedfile!:any;

  constructor(private formBuilder: FormBuilder, 
    private api : UserserviceService, private route: ActivatedRoute, private http : HttpClient) { }

  ngOnInit(): void {
    this.userid = this.route.snapshot.params['id'];
    this.formvalue = this.formBuilder.group({
      firstname:[' ',[Validators.required]],
      lastname:[' ',[Validators.required]],
      dob:[' ',[Validators.required]],
      email:[' ',[Validators.required]],
      password:[' ',[Validators.required]],
      confirmpassword:[' ',[Validators.required]],
      gender:['',[Validators.required]],
      address:[' ',[Validators.required]],
      img:['']


    })
    this.getUserDetails();
    this.getdata();
  }

  postUserDetails(){
    this.usermodel.firstname = this.formvalue.value.firstname;
    this.usermodel.lastname = this.formvalue.value.lastname;
    this.usermodel.dob = this.formvalue.value.dob;
    this.usermodel.email = this.formvalue.value.email;
    this.usermodel.password = this.formvalue.value.password;
    this.usermodel.confirmpassword = this.formvalue.value.confirmpassword;
    this.usermodel.gender = this.formvalue.value.gender;
    this.usermodel.address = this.formvalue.value.address;


    this.api.postUser(this.usermodel)
    .subscribe(res=>{
      console.log(res);
      alert("User Added Successfully")
      let ref= document.getElementById('cancel')
      ref?.click();
      this.formvalue.reset();
    },
    err=>{
      alert("Something went wrong")
    }
    )
  }

  getUserDetails(){
    this.api.getUser()
    .subscribe(res=>{
      this.userdata = res;
    })
  }

  deleteUser(data:any){

    this.api.deleteUser(data.id)
    .subscribe(res=>{
      alert("User Deleted")
      this.getUserDetails();
    })
  }

  edit(data:any){
    this.showadd= false;
    this.showupdate=true;
    this.usermodel.id = data.id;
    this.formvalue.controls['firstname'].setValue(data.firstname)
    this.formvalue.controls['lastname'].setValue(data.lastname)
    this.formvalue.controls['dob'].setValue(data.dob)
    this.formvalue.controls['email'].setValue(data.email)
    this.formvalue.controls['gender'].setValue(data.gender)
    this.formvalue.controls['address'].setValue(data.address)


  }

  updateUserdata(){
    this.usermodel.firstname = this.formvalue.value.firstname;
    this.usermodel.lastname = this.formvalue.value.lastname;
    this.usermodel.dob = this.formvalue.value.dob;
    this.usermodel.email = this.formvalue.value.email;
    this.usermodel.password = this.formvalue.value.password;
    this.usermodel.confirmpassword = this.formvalue.value.confirmpassword;
    this.usermodel.gender = this.formvalue.value.gender;
    this.usermodel.address = this.formvalue.value.address;

    this.api.UpdateUser(this.usermodel, this.usermodel.id)

    .subscribe(res=>{
      alert("Update Sucessfully");
      let ref= document.getElementById('cancel')
      ref?.click();
      this.formvalue.reset();
      this.api.getUser();
    })
  }

  clickAdd(){
    this.formvalue.reset();
    this.showadd= true;
    this.showupdate=false;
  }


  getdata(){
    this.api.getbyId(this.userid).subscribe(res=>{
      this.user=res;
    })

  }


  
  onFileSelect(event:any){

    this.selectedfile = event.target.files[0];
  }
  upload(){

    const fd = new FormData();
    fd.append('img', this.selectedfile, this.selectedfile.name);
    this.http.post('http://localhost:3000/signup',fd)
    .subscribe(res=>{
      console.log(res);
    })
  } 

}
