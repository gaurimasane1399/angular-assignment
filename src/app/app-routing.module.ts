import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { UserlistComponent } from './userlist/userlist.component';
import { UsercardComponent } from './usercard/usercard.component';
import { DetailsComponent } from './details/details.component';
const routes: Routes = [

  {
    path:'login',
    component: LoginComponent
  },
  {
    path:'signup',
    component: SignupComponent
  },
  {
    path:'userlist',
    component: UserlistComponent
  },
  {
    path:'usercard',
    component: UsercardComponent
  },
  {
    path:'details/:id',
    component: DetailsComponent
  },
  {
    path:'',
    redirectTo: 'login', pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
