import { Directive, Input } from "@angular/core";
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from "@angular/forms";

@Directive({
    selector: '[appconfirmValidator]',
    providers: [{
        provide :NG_VALIDATORS,
        useExisting: validatorpassword,
        multi: true
    }]
})
export class validatorpassword implements Validator{
    @Input()
    appconfirmValidator!: string;
    validate(control: AbstractControl): {[key:string]:any} | null {
        const controlToCompare = control.parent?.get(this.appconfirmValidator);
        if(controlToCompare && controlToCompare.value!== control.value){
            return {
                'notEqual' : true
            };
        }
        return null;
    }

}