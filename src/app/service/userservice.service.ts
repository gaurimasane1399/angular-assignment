import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  constructor(private http : HttpClient) { }

  postUser(data:any){
    return this.http.post<any>("http://localhost:3000/signup",data)
    .pipe(map((res:any)=>{
      return res;
    }))

  }
    getUser(){
      return this.http.get<any>("http://localhost:3000/signup")
      .pipe(map((res:any)=>{
        return res;
      }))
  }
  UpdateUser(data:any, id:number){
    return this.http.put<any>("http://localhost:3000/signup/"+id,data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }

  deleteUser(id: number){
    return this.http.delete<any>("http://localhost:3000/signup/"+id)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  getbyId(id:number){
    return this.http.get<any>("http://localhost:3000/signup/"+id)
    .pipe(map((res:any)=>{
      return res;
    }))
  } 
}
