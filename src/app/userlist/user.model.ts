export class UserModel{
    id: number = 0;
    firstname: string ='';
      lastname: string = '';
      dob: string = '';
      email: string = '';
      password: string= '';
      confirmpassword: string = '';
      gender : string ='';
      address : string = '';
      img:string='';
}